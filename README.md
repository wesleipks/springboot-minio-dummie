# Minio Dummie

Projeto para testes de comunicacao spring boot e minio...

## Dependencias
- Java 8
- Projeto Lombok
- Minio

## Minio server

Executar na raiz da pasta 'docker'. Comandos:

    docker-compose up -d
    
## Referencias
- [minio-limits](https://github.com/minio/minio/blob/master/docs/minio-limits.md)
- [jlefebure/spring-boot-starter-minio](https://github.com/jlefebure/spring-boot-starter-minio)