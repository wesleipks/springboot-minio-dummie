package wes.exemplo.miniodummie.service;

import io.minio.BucketExistsArgs;
import io.minio.GetObjectArgs;
import io.minio.ListObjectsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.Result;
import io.minio.messages.Item;
import io.minio.messages.NotificationRecords;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import wes.exemplo.miniodummie.ExcecaoRuntimeQualquer;
import wes.exemplo.miniodummie.notification.MinioNotification;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MinioService {

    private static final Logger logger = LoggerFactory.getLogger(MinioService.class.getName());

    @Autowired
    MinioClient minioClient;

    @Value("${minio.bucket.name}")
    String defaultBucketName;

    @Value("${minio.default.folder}")
    String defaultBaseFolder;

    public List<String> getAllBuckets() {
        try {
            return minioClient.listBuckets()
                    .stream()
                    .map(e -> e.name() + " :: " + e.creationDate().withZoneSameInstant(ZoneId.of("America/Sao_Paulo")))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new ExcecaoRuntimeQualquer(e.getMessage());
        }
    }

    public void uploadFile(String name, byte[] content) {
        try (InputStream inputStream = new ByteArrayInputStream(content)) {
            if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(defaultBucketName).build())) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(defaultBucketName).build());
            }
            minioClient.putObject(PutObjectArgs
                    .builder()
                    .bucket(defaultBucketName)
                    .object(name)
                    .stream(inputStream, content.length, -1)
                    .build());
        } catch (Exception e) {
            throw new ExcecaoRuntimeQualquer(e.getMessage());
        }
    }

    public byte[] getFile(String key) {
        try {
            InputStream objeto = minioClient.getObject(GetObjectArgs.builder()
                    .bucket(defaultBucketName)
                    .object(key)
                    .build());
            byte[] content = IOUtils.toByteArray(objeto);
            objeto.close();
            return content;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public List<Item> getListaObjetos() {
        return getItems(minioClient.listObjects(ListObjectsArgs.builder().bucket(defaultBucketName).build()));
    }

    private List<Item> getItems(Iterable<Result<Item>> myObjects) {

        return StreamSupport
                .stream(myObjects.spliterator(), true)
                .map(itemResult -> {
                    try {
                        return itemResult.get();
                    } catch (Exception e) {
                        throw new ExcecaoRuntimeQualquer("Erro recuperando lista de objetos...", e);
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     * Supported Event Types
     * s3:ObjectCreated:Put
     * s3:ObjectCreated:CompleteMultipartUpload
     * s3:ObjectAccessed:Head
     * s3:ObjectCreated:Post
     * s3:ObjectRemoved:Delete
     * s3:ObjectCreated:Copy
     * s3:ObjectAccessed:Get
     *
     * @param notificationRecords ...
     */
    @MinioNotification(value = {"s3:ObjectCreated:*", "s3:ObjectAccessed:*", "s3:ObjectRemoved:Delete"})
    public void handleGetAll(NotificationRecords notificationRecords) {
        logger.info("[#] handleGetAll");
        notificationRecords.events()
                .forEach(e -> logger.info("[#] Evento {} ocorrido as {} para {}/{}", e.eventType(), e.eventTime(), e.bucketName(), e.objectName()));
    }

    @MinioNotification(value = {"s3:ObjectCreated:CompleteMultipartUpload"})
    public void handleGetPartUpload(NotificationRecords notificationRecords) {
        logger.info("[#] handleGetPartUpload : s3:ObjectCreated:CompleteMultipartUpload");
        notificationRecords.events()
                .forEach(e -> logger.info("[#] Evento {} ocorrido as {} para {}/{}", e.eventType(), e.eventTime(), e.bucketName(), e.objectName()));
    }

}
