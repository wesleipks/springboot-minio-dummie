package wes.exemplo.miniodummie.controller;

import io.minio.messages.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import wes.exemplo.miniodummie.service.MinioService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/minio")
public class DummieController {

    @Autowired
    MinioService minioService;

    @GetMapping
    public String dummie() {

        return "..." + LocalDateTime.now();
    }

    @GetMapping(path = "/buckets")
    public List<String> listBuckets() {
        return minioService.getAllBuckets();
    }

    @GetMapping(path = "/uplist")
    public List<Item> upList() {
        return minioService.getListaObjetos();
    }

    @PostMapping(path = "/up", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public Map<String, String> fileUp(@RequestPart(value = "file", required = false) MultipartFile files) throws IOException {
        minioService.uploadFile(files.getOriginalFilename(), files.getBytes());
        Map<String, String> result = new HashMap<>();
        result.put("key", files.getOriginalFilename());
        return result;
    }

    @GetMapping(path = "/down")
    public ResponseEntity<ByteArrayResource> fileDown(@RequestParam(value = "file") String file) {
        byte[] data = minioService.getFile(file);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + file + "\"")
                .body(resource);
    }

}