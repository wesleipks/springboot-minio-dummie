package wes.exemplo.miniodummie;

public class ExcecaoRuntimeQualquer extends RuntimeException {

    public ExcecaoRuntimeQualquer(String errorMessage) {
        super(errorMessage);
    }

    public ExcecaoRuntimeQualquer(String message, Throwable cause) {
        super(message, cause);
    }

}
