package wes.exemplo.miniodummie.config;

import io.minio.CloseableIterator;
import io.minio.ListenBucketNotificationArgs;
import io.minio.MinioClient;
import io.minio.Result;
import io.minio.messages.NotificationRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import wes.exemplo.miniodummie.notification.MinioNotification;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Configuration
public class MinioNotificationConfiguration implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(MinioNotificationConfiguration.class);

    @Value("${minio.bucket.name}")
    String defaultBucketName;

    @Autowired
    private MinioClient minioClient;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        for (String beanName : applicationContext.getBeanDefinitionNames()) {
            Object obj = applicationContext.getBean(beanName);

            Class<?> objClz = obj.getClass();
            if (org.springframework.aop.support.AopUtils.isAopProxy(obj)) {
                objClz = org.springframework.aop.support.AopUtils.getTargetClass(obj);
            }

            setHandler(obj, objClz);
        }
    }

    private void setHandler(Object obj, Class<?> objClz) {
        for (Method m : objClz.getDeclaredMethods()) {
            if (m.isAnnotationPresent(MinioNotification.class)) {
                if (m.getParameterCount() != 1) {
                    throw new IllegalArgumentException("Minio notification handler should have only one NotificationInfo parameter");
                }
                if (m.getParameterTypes()[0] != NotificationRecords.class) {
                    throw new IllegalArgumentException("Parameter should be instance of NotificationRecords");
                }
                MinioNotification annotation = m.getAnnotation(MinioNotification.class);
                Thread handler = hadlerRegister(obj, m, annotation);
                handler.start();
            }
        }
    }

    private Thread hadlerRegister(Object obj, Method m, MinioNotification annotation) {
        return new Thread(() -> {
            for (; ; ) {
                try {
                    String annotationValueAux = Arrays.toString(annotation.value());
                    LOGGER.info("Registering Minio handler on {} with notification {}", m.getName(), annotationValueAux);
                    listenerBucketNotification(obj, m, annotation);
                } catch (Exception e) {
                    LOGGER.error("Error while registering notification for method {} with notification {}", m.getName(), Arrays.toString(annotation.value()));
                    throw new IllegalStateException("Cannot register handler", e);
                }
            }
        });
    }

    private void listenerBucketNotification(Object obj, Method m, MinioNotification annotation) throws IOException, io.minio.errors.ErrorResponseException, io.minio.errors.InsufficientDataException, io.minio.errors.InternalException, io.minio.errors.InvalidBucketNameException, java.security.InvalidKeyException, io.minio.errors.InvalidResponseException, java.security.NoSuchAlgorithmException, io.minio.errors.ServerException, io.minio.errors.XmlParserException {
        try (CloseableIterator<Result<NotificationRecords>> list = minioClient.listenBucketNotification(ListenBucketNotificationArgs.builder()
                .bucket(defaultBucketName)
                .prefix(annotation.prefix())
                .suffix(annotation.suffix())
                .events(annotation.value())
                .build())) {
            while (list.hasNext()) {
                NotificationRecords info = list.next().get();
                try {
                    LOGGER.debug("Receive notification for method {}", m.getName());
                    m.invoke(obj, info);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    LOGGER.error("Error while handling notification for method {} with notification {}", m.getName(), Arrays.toString(annotation.value()));
                    LOGGER.error("Exception is", e);
                }
            }
        }
    }
}